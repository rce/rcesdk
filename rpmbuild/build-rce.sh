#!/bin/sh

echo '##### build tools #####'
rpmbuild -bb SPECS/rcetools.spec
sudo rpm -i RPMS/armv7hl/rce_tools-2.4.2-1.el7.armv7hl.rpm

echo '##### build sdk #####'
rpmbuild -bb SPECS/sdk.spec

