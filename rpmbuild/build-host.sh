#!/bin/sh

echo '##### build crosstool-ng #####'
rpmbuild -bb SPECS/crosstool-ng.spec
sudo rpm -i RPMS/x86_64/rce_crosstool-ng-20180820.git-1.el7.x86_64.rpm

echo '##### build cross-arm #####'
rpmbuild -bb SPECS/arm-crosstool-gcc.spec
sudo rpm -i RPMS/x86_64/rce_cross-arm-gcc7-7.3-1.el7.x86_64.rpm

echo '##### build kernel #####'
rpmbuild --target noarch -bb SPECS/kernel.spec
sudo rpm -i RPMS/noarch/rce_xilinx_kernel_4.14.0-4.14.0-1.el7.armv7hl.noarch.rpm 
echo '##### build drivers #####'
rpmbuild --target noarch -bb SPECS/rcedriver.spec
sudo rpm -i RPMS/noarch/rce_drivers-5.4.0.4.14.0-1.el7.armv7hl.noarch.rpm


echo '##### build tools #####'
rpmbuild -bb SPECS/rcetools.spec
sudo rpm -i RPMS/x86_64/rce_tools-2.4.2-1.el7.x86_64.rpm

echo '##### build setup #####'
rpmbuild --target noarch -bb SPECS/setup.spec
sudo rpm -i RPMS/noarch/rce_setup-1.0.0-1.el7.noarch.rpm 

echo '##### build sdk #####'
rpmbuild -bb SPECS/sdk.spec
sudo rpm -i RPMS/x86_64/rce_sdk-1.0.0-2.el7.x86_64.rpm 

