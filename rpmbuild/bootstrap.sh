#!/bin/sh
user=$(whoami)
if [ $user == root ] ; then
echo ERROR: RPMs should not be built as $user
fi


for dir in RPMS SRPMS BUILD BUILDROOT 
do
    if [ ! -d ${dir} ] ; then mkdir ./${dir} ; fi
done

	
if [ ! -e ~/.rpmmacros ] ; then
touch ~/.rpmmacros 
echo '%_build_name_fmt  %%{ARCH}/%%{NAME}-%%{VERSION}-%%{RELEASE}.%{_target_cpu}.rpm' >> ~/.rpmmacros
fi

