#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%if %{_target_cpu} == armv7hl
%define _ext_arch  %{nil}
%else  ${_target_cpu} == noarch
%define _ext_arch .armv7hl
%endif

%define _binaries_in_noarch_packages_terminate_build   0
%define _topdir             %(echo $HOME)/rpmbuild
%define _smp_mflags         -j8
%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
%define _cross /opt/rce/package/x86_64/cross-arm-gcc7/7.3/bin/arm-unknown-linux-gnueabihf-
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    xilinx_kernel
%define version     4.14.0
%define vendor      rce
%define release     1
%define __prefix    /opt/rce/package/armv7hl/%{pkgname}/%{version}

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: xilinx kernel for Zynq
Name: %{vendor}_%{pkgname}_%{version}
Version: %{version}
Release: %{release}.el7%{_ext_arch}
Vendor: %{vendor}
License: GNU
Group: Kernel
Source: http://crosstool-ng.org
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel, rce_cross-arm-gcc7 == 7.3
Prefix: %{__prefix}
AutoReq: no
BuildArch: noarch
%description
Xilinx Linux kernel

%changelog
* Tue Aug 21 2018 wittgen <wittgen@slac.stanford.edu>  [2018.2]
- Initial RPM releas

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------

%prep
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p  $RPM_BUILD_ROOT/
cd $RPM_BUILD_ROOT/
git clone -b slac-%{version} https://github.com/slaclab/linux-xlnx.git
cd $RPM_BUILD_ROOT/linux-xlnx
cp %{_topdir}/SOURCES/zynqrce_defconfig $RPM_BUILD_ROOT/linux-xlnx/arch/arm/configs/

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build
%define _cross_prefix /opt/rce/rootfs/centos7
cd $RPM_BUILD_ROOT/linux-xlnx
rm -f .config
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/images
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/dtb
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/build
make ARCH=arm  KBUILD_DEFCONFIG=zynqrce_defconfig defconfig
make ARCH=arm CROSS_COMPILE=%{_cross} LOADADDR=0x8000 uImage -j24
make ARCH=arm CROSS_COMPILE=%{_cross} modules -j24
make ARCH=arm CROSS_COMPILE=%{_cross} dtbs
make ARCH=arm  CROSS_COMPILE=%{_cross} EXTRA_CFLAGS="-B%{_cross_prefix}/opt/rh/devtoolset-7/root/lib/gcc/armv7hl-redhat-linux-gnueabi/7  -isystem %{_cross_prefix}//usr/include" LDFLAGS="-B %{_cross_prefix}/opt/rh/devtoolset-7/root/lib/gcc/armv7hl-redhat-linux-gnueabi/7 --sysroot=%{_cross_prefix}"  -C tools/perf
dtc -I dts %{_topdir}/SOURCES/zynq-hsio2.dts -O dtb -i $RPM_BUILD_ROOT/linux-xlnx/arch/arm/boot/dts/ -o $RPM_BUILD_ROOT/%{__prefix}/dtb/zynq-hsio2.dtb
cp  $RPM_BUILD_ROOT/linux-xlnx/arch/arm/boot/dts/*.dtb   $RPM_BUILD_ROOT/%{__prefix}/dtb/
dtc -I dts %{_topdir}/SOURCES/zynq-cob.dts -O dtb -i $RPM_BUILD_ROOT/linux-xlnx/arch/arm/boot/dts/ -o $RPM_BUILD_ROOT/%{__prefix}/dtb/zynq-cob.dtb
cp  $RPM_BUILD_ROOT/linux-xlnx/arch/arm/boot/uImage  $RPM_BUILD_ROOT/%{__prefix}/images
cp -r %{_topdir}/SOURCES/boot   $RPM_BUILD_ROOT/%{__prefix}/images

#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------
cd $RPM_BUILD_ROOT/linux-xlnx
make ARCH=arm CROSS_COMPILE=%{_cross} INSTALL_MOD_PATH=$RPM_BUILD_ROOT/%{__prefix} modules_install
make ARCH=arm CROSS_COMPILE=%{_cross} INSTALL_HDR_PATH=$RPM_BUILD_ROOT/%{__prefix} headers_install
make clean
rm -rf $(find  $RPM_BUILD_ROOT/linux-xlnx   -name ".git*" )
cp -r $RPM_BUILD_ROOT/linux-xlnx/* $RPM_BUILD_ROOT/%{__prefix}/build
rm -rf $RPM_BUILD_ROOT/linux-xlnx
rm -f $(find $RPM_BUILD_ROOT/${__prefix} -type l)


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/*



