%define pkgname    tools
%define version     2.4.2
%define vendor      rce
%define release     1                                                                                                                                                            
%define __prefix    /opt/rce/package/%{_target_cpu}/%{pkgname}/%{version}
Name: %{vendor}_%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
Summary:        RCE command line tools 
Group:          System Environment/Libraries
License:        SLAC
URL:            https://github.com/slaclab/rce-gen3-sw-lib
BuildRoot:      %{_tmppath}/%{pkgname}-%{version}-%{release}-root
BuildRequires: pth-devel
Requires: pth
Prefix:  %{__prefix}
%description
RCE command line tools

%ifarch x86_64
%define _smpflags -j8
%define rcearch i86-linux-64-opt 
%else
%define  _smpflags -j1
%define rcearch arm-linux-rceCA9-opt
%endif


%prep
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p  $RPM_BUILD_ROOT/
cd $RPM_BUILD_ROOT/
git clone -b b2.4.2 https://github.com/slaclab/rce-gen3-sw-lib.git

%build
cd $RPM_BUILD_ROOT/rce-gen3-sw-lib
make verbose=true %{rcearch} CROSS_COMPILE=/usr/bin/ PTH_ROOT=/usr
mkdir -p  $RPM_BUILD_ROOT/%{__prefix}
cd $RPM_BUILD_ROOT/rce-gen3-sw-lib/build/%{rcearch}
cp -rL include bin lib  $RPM_BUILD_ROOT/%{__prefix}
cp %{_topdir}/SOURCES/rced.sh  $RPM_BUILD_ROOT/%{__prefix}/bin/
rm -rf $RPM_BUILD_ROOT/rce-gen3-sw-lib


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{__prefix}/lib/*
%{__prefix}/include/*
%{__prefix}/bin/*

%post
%ifarch armv7hl
cp  %{__prefix}/bin/rced.sh  /sbin/
%endif



%changelog
* Wed Aug  29 2018 Matthias Wittgen <wittgen@slac.stanford.edu> - 2.4.2
- Initial build
