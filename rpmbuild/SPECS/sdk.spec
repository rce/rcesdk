#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------

%define _binaries_in_noarch_packages_terminate_build   0
%define _topdir             %(echo $HOME)/rpmbuild
%define _smp_mflags         -j8
%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}

%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    sdk
%define version    1.0.0
%define vendor     rce
%define release    2
%define __prefix   /opt/rce/package/%{_target_cpu}/%{pkgname}/%{version}


#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: RCE SDK
Name: %{vendor}_%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
License: GNU
Group: RCE SDK
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
Prefix: %{__prefix}
AutoReq: no
Requires: pyparted  
Requires: python-beautifulsoup4  
Requires: python-urllib3
Requires: rce_setup >=   1.0.0
Requires: uboot-tools
%ifarch arm
Requires: rce_tools == 2.4.2
Requires: rce_xilinx_kernel_4.14.0 
Requires: rce_drivers
%endif
%ifarch aarch64
Requires: rce_xilinx_kernel_arm64_4.14.0
Requires: rce_drivers_arm64
%endif
%ifarch  x86_64
Requires: dnf
Requires: rce_tools == 2.4.2
Requires: rce_cross-arm-gcc7 == 7.3
Requires: rce_cross-arm64-gcc7 == 7.3
Requires: rce_crosstool-ng ==  20180820.git
Requires: rce_xilinx_kernel_4.14.0 == 4.14.0
Requires: rce_xilinx_kernel_arm64_4.14.0 == 4.14.0
Requires: rce_drivers == 5.5.0.4.14.0
Requires: rce_drivers_arm64 == 5.5.0.4.14.0
%endif



%description
RCE SDK

%changelog
* Wed Aug 29 2018 wittgen <wittgen@slac.stanford.edu>  [14.6]
- Initial RPM release

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build

#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/bin
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/etc
%ifarch x86_64
cp %{_topdir}/SOURCES/tools/scripts/*  $RPM_BUILD_ROOT/%{__prefix}/bin
cp %{_topdir}/SOURCES/tools/qemu-bins/*  $RPM_BUILD_ROOT/%{__prefix}/bin
cp %{_topdir}/SOURCES/tools/etc/*  $RPM_BUILD_ROOT/%{__prefix}/etc
%endif

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/*

%post
%ifarch x86_64
%{__prefix}/bin/qemu-binfmt-conf.sh --systemd arm
/usr/bin/systemctl enable systemd-binfmt
/usr/bin/systemctl restart systemd-binfmt
%endif



