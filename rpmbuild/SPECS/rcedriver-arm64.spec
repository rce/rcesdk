#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define _binaries_in_noarch_packages_terminate_build   0
%define _topdir             %(echo $HOME)/rpmbuild
%if %{_target_cpu} == aarch64
%define _ext_arch  %{nil}
%else  ${_target_cpu} == noarch
%define _ext_arch .aarch64
%endif

%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
%define _cross /opt/rce/package/x86_64/cross-arm64-gcc7/7.3/bin/aarch64-unknown-linux-gnu-
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%define _xilinx_release 4.14.0
%define _driver_ver 5.5.0
#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    drivers
%define version     %{_driver_ver}.%{_xilinx_release}
%define vendor      rce
%define release     1
%define __prefix    /opt/rce/package/aarch64/%{pkgname}/%{version}
%define _kdir /opt/rce/package/aarch64/xilinx_kernel/%{_xilinx_release}/build

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: RCE drivers
Name: %{vendor}_%{pkgname}_arm64
Version: %{version}
Release: %{release}%{_ext_arch}
Vendor: %{vendor}
License: SLAC
Group: Kernel Driver
Source: https://github.com/slaclab/aes-stream-drivers.git
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel, rce_cross-arm64-gcc7 == 7.3
Requires: rce_xilinx_kernel_arm64_4.14.0
Prefix: %{__prefix}
AutoReq: no
%description
Xilinx Linux kernel

%changelog
* Tue Aug 21 2018 wittgen <wittgen@slac.stanford.edu>  [2018.2]
- Initial RPM release

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
rm -rf  $RPM_BUILD_ROOT
mkdir -p  $RPM_BUILD_ROOT
cd $RPM_BUILD_ROOT/
git clone -b arm64 https://github.com/slaclab/aes-stream-drivers.git

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build
cd $RPM_BUILD_ROOT/aes-stream-drivers/rce_memmap/driver
make COMP=/opt/rce/package/x86_64/cross-arm64-gcc7/7.3/bin/aarch64-unknown-linux-gnu- KDIR=%{_kdir} ARCH=arm64
cd $RPM_BUILD_ROOT/aes-stream-drivers/rce_stream/driver
make COMP=/opt/rce/package/x86_64/cross-arm64-gcc7/7.3/bin/aarch64-unknown-linux-gnu- KDIR=%{_kdir} ARCH=arm64

#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------
mkdir  -p $RPM_BUILD_ROOT/%{__prefix}
cp $(find  $RPM_BUILD_ROOT/aes-stream-drivers -name "*.ko") $RPM_BUILD_ROOT/%{__prefix}/
rm -rf  $RPM_BUILD_ROOT/aes-stream-drivers
rm -rf  $RPM_BUILD_ROOT/linux-xlnx


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/*


