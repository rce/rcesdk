#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%if %{_target_cpu} == armv7hl
%define _ext_arch  %{nil}
%else  ${_target_cpu} == noarch
%define _ext_arch .armv7hl
%endif

%define _binaries_in_noarch_packages_terminate_build   0
%define _topdir             %(echo $HOME)/rpmbuild
%define _smp_mflags         -j8
%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
##%define _cross /opt/rce/package/x86_64/cross-arm-eabi-gcc/7.3/bin/arm-unknown-eabi-
%define _cross  /opt/Xilinx/SDK/2018.1/gnu/aarch32/lin/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf-
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%define cross_prefix /opt/rce/package/x86_64/cross-arm-eabi-gcc/7.3/bin/arm-unknown-eabi-
#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    xlnx-uboot-slac
%define version    14.6
%define vendor     rce
%define release    1
%define __prefix   /opt/rce/package/armv7hl/%{pkgname}/%{version}

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: xilinx kernel for Zynq
Name: %{vendor}-%{pkgname}
Version: %{version}
Release: %{release}.el7%{_ext_arch}
Vendor: %{vendor}
License: GNU
Group: Kernel
Source: http://crosstool-ng.org
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel, rce-cross-arm-eabi-gcc == 7.3
Prefix: %{__prefix}
AutoReq: no
BuildArch: noarch
%description
Xilinx uboot for RCE

%changelog
* Tue Aug 28 2018 wittgen <wittgen@slac.stanford.edu>  [14.6]
- Initial RPM releas

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p  $RPM_BUILD_ROOT/
cd $RPM_BUILD_ROOT/
git clone -b xilinx-rce-v14.6.01  https://github.com/slaclab/uboot-xlnx-rce.git
git clone -b b2.4.2 https://github.com/slaclab/rce-gen3-sw-lib.git

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build
cd $RPM_BUILD_ROOT/rce-gen3-sw-lib
make CROSS_COMPILE=%{cross_prefix} verbose=true arm-eabi-rceCA9-opt
cd $RPM_BUILD_ROOT/uboot-xlnx-rce
make CC=%{cross_prefix}gcc  AR=%{cross_prefix}ar  LD=%{cross_prefix}ld OBJCOPY=%{cross_prefix}objcopy NM=%{cross_prefix}nm zynq_rce_config
make CC=%{cross_prefix}gcc  AR=%{cross_prefix}ar  LD=%{cross_prefix}ld OBJCOPY=%{cross_prefix}objcopy NM=%{cross_prefix}nm DAT_ROOT=$RPM_BUILD_ROOT/rce-gen3-sw-lib 
cd $RPM_BUILD_ROOT/rce-gen3-sw-lib/bootstrap 
make CROSS_COMPILE=%{cross_prefix} verbose=true arm-eabi-rceCA9-opt


#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/images
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/bin
cp $RPM_BUILD_ROOT/uboot-xlnx-rce/u-boot* $RPM_BUILD_ROOT/%{__prefix}/images
cp $RPM_BUILD_ROOT/uboot-xlnx-rce/tools/mkenvimage $RPM_BUILD_ROOT/%{__prefix}/bin
#rm -rf  $RPM_BUILD_ROOT/rce-gen3-sw-lib $RPM_BUILD_ROOT/uboot-xlnx-rce

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/images
%{__prefix}/bin



