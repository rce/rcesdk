#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define _binaries_in_noarch_packages_terminate_build   0
%define _topdir             %(echo $HOME)/rpmbuild
%if %{_target_cpu} == armv7hl
%define _ext_arch  %{nil}
%else  ${_target_cpu} == noarch
%define _ext_arch .armv7hl
%endif

%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
%define _cross /opt/rce/package/x86_64/cross-arm-gcc7/7.3/bin/arm-unknown-linux-gnueabihf-
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%define _xilinx_release 4.14.0
%define _driver_ver 5.5.0
#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    drivers
%define version     %{_driver_ver}.%{_xilinx_release}
%define vendor      rce
%define release     1
%define __prefix    /opt/rce/package/armv7hl/%{pkgname}/%{version}
%define _kdir /opt/rce/package/armv7hl/xilinx_kernel/%{_xilinx_release}/build

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: RCE drivers
Name: %{vendor}_%{pkgname}
Version: %{version}
Release: %{release}.el7%{_ext_arch}
Vendor: %{vendor}
License: SLAC
Group: Kernel Driver
Source: https://github.com/slaclab/aes-stream-drivers.git
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel, rce_cross-arm-gcc7 == 7.3
Requires: rce_xilinx_kernel_%{_xilinx_release} == %{_xilinx_release}
Prefix: %{__prefix}
AutoReq: no
%description
Xilinx Linux kernel

%changelog
*  Tue May 7 2019 wittgen <wittgen@slac.stanford.edu>  [4.14.0]
- Initial RPM release

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
rm -rf  $RPM_BUILD_ROOT
mkdir -p  $RPM_BUILD_ROOT
cd $RPM_BUILD_ROOT/
git clone -b v%{_driver_ver} https://github.com/slaclab/aes-stream-drivers.git

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build
cd $RPM_BUILD_ROOT/aes-stream-drivers/rce_memmap/driver
make COMP=%{_cross} KDIR=%{_kdir}
cd $RPM_BUILD_ROOT/aes-stream-drivers/rce_stream/driver
make COMP=%{_cross} KDIR=%{_kdir}

#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------
mkdir  -p $RPM_BUILD_ROOT/%{__prefix}/include
cp $(find  $RPM_BUILD_ROOT/aes-stream-drivers -name "*.ko") $RPM_BUILD_ROOT/%{__prefix}/
cp  -r  $RPM_BUILD_ROOT/aes-stream-drivers/include/*  $RPM_BUILD_ROOT/%{__prefix}/include
rm -rf  $RPM_BUILD_ROOT/aes-stream-drivers
rm -rf  $RPM_BUILD_ROOT/linux-xlnx


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/*


