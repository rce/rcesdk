#----------------------------------------------------------
# rpmbuild -bb --clean python.spec
#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define _pythonlibs  parse cython Pyro4 PyYAML click ipython Pyro4 numpy pyrecord zmq gitpython PyGithub python-augeas pycurl pyparted
%define _topdir             %(echo $HOME)/rpmbuild
%ifarch x86_64
%define _smp_mflags         -j8
%define _extrapythonlibs  PyQt5 
%else
%define  _smp_mflags         -j1
%define _extrapythonlibs %{nil}
%endif
%define __arch_install_post  \
    /usr/lib/rpm/redhat/brp-strip %{__strip} \
    /usr/lib/rpm/check-buildroot \
    %{nil}
%define debug_package       %{nil}
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    python
%define version     3.6.6
%define libvers     3.6
%define vendor      rce
%define release     1
%define __prefix    /opt/rce/package/%{_target_cpu}/%{pkgname}/%{version}

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: An interpreted, interactive, object-oriented programming language.
Name: %{vendor}-%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
License: PSF
Group: Development/Languages
Source: https://www.python.org/ftp/python/%{version}/Python-%{version}.tgz
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel
BuildRequires: bzip2-devel
BuildRequires: db4-devel
BuildRequires: expat-devel
BuildRequires: openssl-devel
BuildRequires: readline-devel
BuildRequires: sqlite-devel
BuildRequires: zlib-devel
BuildRequires: zeromq-devel
BuildRequires: libcurl-devel
BuildRequires: parted-devel
Prefix: %{__prefix}
AutoReq: no
Requires: zeromq
Requires: qt5-qtbase 
Requires: parted
%description
Python

%changelog
* Sun Aug 12 2018 wittgen <wittgen@slac.stanford.edu>  [3.6.6-1]
- Initial RPM release

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
%setup -n Python-%{version}

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build

#--- CONFIGURE
./configure \
    --enable-shared \
    --prefix=%{__prefix}

#--- MAKE
make %{?_smp_mflags}

#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------
%install

#[ -d "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

#--- MAKE INSTALL
make install DESTDIR=$RPM_BUILD_ROOT
export PYCURL_SSL_LIBRARY=nss
export PYTHONPATH=$RPM_BUILD_ROOT/%{__prefix}/lib/python3.6/site-packages:%{__prefix}/lib/python3.6/site-packages
export LD_LIBRARY_PATH=$RPM_BUILD_ROOT/%{__prefix}/lib
$RPM_BUILD_ROOT/%{__prefix}/bin/python3 -m ensurepip --root $RPM_BUILD_ROOT/%{__prefix}
export LDFLAGS="-L$RPM_BUILD_ROOT/%{__prefix}/lib"
$RPM_BUILD_ROOT/%{__prefix}/bin/python3 -m pip install --no-cache-dir --prefix $RPM_BUILD_ROOT/%{__prefix}  --ignore-installed %{_extrapythonlibs} %{_pythonlibs}

find   $RPM_BUILD_ROOT -type f -print0 |
       xargs -0 grep -l %{__prefix}/bin/python |
       while read file
       do
          sed -i 's|^#!.*python|#!/usr/bin/env python'""'|'  $file
       done
chmod +w  $(find $RPM_BUILD_ROOT/%{__prefix} -name "*.so" -or -name "*.a")
find $RPM_BUILD_ROOT/%{__prefix} -name "*.so" -exec strip {} \;
find $RPM_BUILD_ROOT/%{__prefix} -name "*.a" -exec strip {} \;
cd $RPM_BUILD_ROOT/%{__prefix}/lib && ln -s python3.6 python
cd $RPM_BUILD_ROOT/%{__prefix}/include &&  ln -s python3.6m python3.6


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/bin/*
%{__prefix}/lib*
%{__prefix}/include/*
%{__prefix}/share/*


