%define __spec_install_pre /bin/true
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%define pkgname        rogue
%define version     2.12.0
%define vendor      rce
%define release     1                                                                                                                                                            
%define __prefix    /opt/rce/package/%{_target_cpu}/%{pkgname}/%{version}
Name: %{vendor}-%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
Summary:        The rogue python DAQ framework
Group:          System Environment/Libraries
License:        SLAC
URL:            https://github.com/slaclab/rogue/
#Source0:        
BuildRoot:      %{_tmppath}/%{pkgname}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake3, bzip2-devel, rce-python == 3.6.6, rce-boost == 1.68.0
Requires: bzip2, rce-python == 3.6.6, rce-boost == 1.68.0
%description
Boost build for SLAC rogue

%ifarch x86_64
%define _smpflags -j8
%else
%define  _smpflags -j1
%endif

%prep
rm -rf  $RPM_BUILD_ROOT
mkdir $RPM_BUILD_ROOT
cd $RPM_BUILD_ROOT
git clone --recurse-submodules -b v%{version} https://github.com/slaclab/rogue.git


%build
export PATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/bin:$PATH
export LD_LIBRARY_PATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/lib
export PYTHONPATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/lib/python3.6
export BOOST_ROOT=/opt/rce/package/%{_target_cpu}/boost/1.68.0
export BOOST_INCLUDE=/opt/rce/package/%{_target_cpu}/boost/1.68.0/include
export BOOST_LIBDIR=/opt/rce/package/%{_target_cpu}/boost/1.68.0/lib
export BOOST_OPTS="-DBOOST_ROOT=${BOOST_ROOT} -DBOOST_INCLUDEDIR=${BOOST_INCLUDE} -DBOOST_LIBRARYDIR=${BOOST_LIBDIR}"

cd  $RPM_BUILD_ROOT/rogue
cmake3  ${BOOST_OPTS} .
make %{_smpflags}
mkdir -p $RPM_BUILD_ROOT/%{__prefix}
cp -r include lib python $RPM_BUILD_ROOT/%{__prefix}
rm -rf  $RPM_BUILD_ROOT/rogue

%install




%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%post 

%postun 

%files
%defattr(-,root,root)
%{__prefix}/lib/*
%{__prefix}/python/*
%{__prefix}/include/*

%changelog
* Thu Aug  16 2018 Matthias Wittgen <wittgen@slac.stanford.edu> - 2.12
- Initial build
