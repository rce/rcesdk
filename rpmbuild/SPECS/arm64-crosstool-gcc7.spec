#----------------------------------------------------------
# rpmbuild -bb --clean python.spec
#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define _topdir             %(echo $HOME)/rpmbuild
%define _crossng_ver 20180820.git
%ifarch x86_64
%define _smp_mflags         -j8
%else
%define  _smp_mflags         -j1
%endif
%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    cross-arm64-gcc7
%define version     7.3
%define vendor      rce
%define release     1
%define __prefix    /opt/rce/package/%{_arch}/%{pkgname}/%{version}

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: A versatile (cross) toolchain generator.
Name: %{vendor}-%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
License: GNU
Group: Development/Languages
Source: somesource 
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel, rce-crosstool-ng == 20180820.git
Prefix: %{__prefix}
AutoReq: no
%description
arm64 cross compiler

%changelog
* Mon Aug 20 2018 wittgen <wittgen@slac.stanford.edu>  [7.3]
- Initial RPM releas

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
mkdir -p  $RPM_BUILD_ROOT/build
cd $RPM_BUILD_ROOT/build
DEFCONFIG=%{_topdir}/SOURCES/gcc7-arm64-cross.defconfig /opt/rce/package/%{_arch}/crosstool-ng/%{_crossng_ver}/bin/ct-ng defconfig 

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build
cd $RPM_BUILD_ROOT/build
unset LD_LIBRARY_PATH
CT_PREFIX=$RPM_BUILD_ROOT/%{__prefix} /opt/rce/package/%{_arch}/crosstool-ng/%{_crossng_ver}/bin/ct-ng build.24
rm -rf $RPM_BUILD_ROOT/build*
chmod +w  $RPM_BUILD_ROOT/%{__prefix}
chmod +w  $RPM_BUILD_ROOT/%{__prefix}/*.bz2 && rm -rf  $RPM_BUILD_ROOT/%{__prefix}/*.bz2
chmod -w $RPM_BUILD_ROOT/%{__prefix}
rm -rf  $RPM_BUILD_ROOT/.config*
rm -rf  $RPM_BUILD_ROOT/.build*


#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------



%clean
chmod -R +w $RPM_BUILD_ROOT
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/*


