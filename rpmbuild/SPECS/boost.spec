%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%define pkgname    boost
%define version     1.68.0
%define vendor      rce
%define release     1                                                                                                                                                            
%define __prefix    /opt/rce/package/%{_target_cpu}/%{pkgname}/%{version}
Name: %{vendor}-%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
Summary:        The Boost C++ headers and shared development libraries
Group:          System Environment/Libraries
License:        Boost
URL:            http://www.boost.org/
Source0:        boost_1_68_0.tar.gz
BuildRoot:      %{_tmppath}/%{pkgname}-%{version}-%{release}-root
BuildRequires:  bzip2-devel, rce-python == 3.6.6
Prefix:  %{__prefix}
Requires: bzip2, rce-python == 3.6.6
%description
Boost build for SLAC rogue

%ifarch x86_64
%define _smpflags -j8
%else
%define  _smpflags -j1
%endif


%prep
%setup -q -n boost_1_68_0

%build
INSTALL_DIR=%{__prefix}
BOOST_ROOT=`pwd`
export BOOST_ROOT
export PATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/bin:$PATH
export LD_LIBRARY_PATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/lib
export PYTHONPATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/lib/python3.6
./bootstrap.sh --prefix=$RPM_BUILD_ROOT/$INSTALL_DIR --with-toolset=gcc --with-icu --with-python=/opt/rce/package/%{_target_cpu}/python/3.6.6/bin/python3  --with-python-version=3.6

%install
export PATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/bin:$PATH
export LD_LIBRARY_PATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/lib
export PYTHONPATH=/opt/rce/package/%{_target_cpu}/python/3.6.6/lib/python3.6
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/$INSTALL_DIR
cat project-config.jam
./b2  %{_smpflags} install

%clean
rm -rf $RPM_BUILD_ROOT

%post 

%postun 

%files
%defattr(-,root,root)
%{__prefix}/lib/*
%{__prefix}/include/*

%changelog
* Fri Aug  10 2018 Matthias Wittgen <wittgen@slac.stanford.edu> - 1.68.0
- Initial build
