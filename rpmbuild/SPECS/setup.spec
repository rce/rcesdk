#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------

%define _binaries_in_noarch_packages_terminate_build   0
%define _topdir             %(echo $HOME)/rpmbuild
%define _smp_mflags         -j8
%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
##%define _cross /opt/rce/package/x86_64/cross-arm-eabi-gcc/7.3/bin/arm-unknown-eabi-
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    setup
%define version    1.0.0
%define vendor     rce
%define release    3
%define __prefix   /opt/rce

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: xilinx kernel for Zynq
Name: %{vendor}_%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
License: GNU
Group: Kernel
Source: http://crosstool-ng.org
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
Prefix: %{__prefix}
AutoReq: no
BuildArch: noarch
%description
Generic setup scripts for RCE SDK

%changelog
* Wed Aug 29 2018 wittgen <wittgen@slac.stanford.edu>  [14.6]
- Initial RPM releas

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build

#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/etc
cp %{_topdir}/SOURCES/setup/*.json  $RPM_BUILD_ROOT/%{__prefix}/etc
cp %{_topdir}/SOURCES/setup/*.py $RPM_BUILD_ROOT/%{__prefix}/
cp %{_topdir}/SOURCES/setup/*.sh $RPM_BUILD_ROOT/%{__prefix}/

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/*




