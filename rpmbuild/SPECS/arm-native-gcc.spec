#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define _unpackaged_files_terminate_build 0
%define _hostarch x86_64
%define _topdir             %(echo $HOME)/rpmbuild
%define _crossng_ver 20180820.git
%define _smp_mflags         -j8
%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    native-arm-gcc
%define version     7.3
%define vendor      rce
%define release     1
%define __prefix    /opt/rce/package/%{_target_cpu}/%{pkgname}/%{version}

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: A versatile (cross) toolchain generator.
Name: %{vendor}-%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
License: PSF
Group: Development/Languages
Source: http://crosstool-ng.org
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel, rce-crosstool-ng == 20180820.git, rce-cross-arm-gcc == 7.3
Prefix: %{__prefix}
AutoReq: no

%description
arm native compiler

%changelog
* Mon Aug 20 2018 wittgen <wittgen@slac.stanford.edu>  [7.3-1]
- Initial RPM releas

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
mkdir -p  $RPM_BUILD_ROOT/build
cd $RPM_BUILD_ROOT/build
DEFCONFIG=%{_topdir}/SOURCES/gcc7-arm-canadian.defconfig /opt/rce/package/%{_hostarch}/crosstool-ng/%{_crossng_ver}/bin/ct-ng defconfig 

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build
unset LD_LIBRARY_PATH
export PATH=/opt/rce/package/%{_hostarch}/cross-arm-gcc/7.3/bin:$PATH
#export LD_LIBRARY_PATH=/opt/rce/package/%{_hostarch}/cross-arm-gcc/7.3/lib:$LD_LIBRARY_PATH
cd $RPM_BUILD_ROOT/build
CT_PREFIX=$RPM_BUILD_ROOT/%{__prefix} /opt/rce/package/%{_hostarch}/crosstool-ng/%{_crossng_ver}/bin/ct-ng build.24
rm -rf $RPM_BUILD_ROOT/build*
chmod +w  $RPM_BUILD_ROOT/%{__prefix}
chmod +w  $RPM_BUILD_ROOT/%{__prefix}/*.bz2 && rm -rf  $RPM_BUILD_ROOT/%{__prefix}/*.bz2
chmod -w $RPM_BUILD_ROOT/%{__prefix}
rm -rf  $RPM_BUILD_ROOT/.config*
rm -rf  $RPM_BUILD_ROOT/.build*


#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------



%clean
chmod -R +w $RPM_BUILD_ROOT/ && rm -rf $RPM_BUILD_ROOT/

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/*


