#----------------------------------------------------------
# rpmbuild -bb --clean python.spec
#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define _topdir             %(echo $HOME)/rpmbuild
%ifarch x86_64
%define _smp_mflags         -j8
%else
%define  _smp_mflags         -j1
%endif
%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    crosstool-ng
%define version     20180820.git
%define vendor      rce
%define release     1
%define __prefix    /opt/rce/package/%{_arch}/%{pkgname}/%{version}

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: A versatile (cross) toolchain generator.
Name: %{vendor}_%{pkgname}
Version: %{version}
Release: %{release}.el7
Vendor: %{vendor}
License: PSF
Group: Development/Languages
Source: https://github.com/crosstool-ng/crosstool-ng.git
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel
Prefix: %{__prefix}
AutoReq: no
%description
Python

%changelog
* Mon Aug 20 2018 wittgen <wittgen@slac.stanford.edu>  [git-master]
- Initial RPM releas

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------
%prep
mkdir -p $RPM_BUILD_ROOT
cd $RPM_BUILD_ROOT
git clone https://github.com/crosstool-ng/crosstool-ng.git 
cd crosstool-ng
git checkout d5900debd397b8909d9cafeb9a1093fb7a5dc6e6

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build
cd $RPM_BUILD_ROOT/crosstool-ng
./bootstrap
./configure \
    --prefix=%{__prefix}

#--- MAKE
make %{?_smp_mflags}
mkdir -p $RPM_BUILD_ROOT/%{__prefix}
make install  DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT/crosstool-ng

#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------



%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/bin/*
%{__prefix}/libexec/*
%{__prefix}/share/*


