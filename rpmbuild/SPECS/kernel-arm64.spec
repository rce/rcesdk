#----------------------------------------------------------
#--- GLOBAL-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%if %{_target_cpu} == aarch64
%define _ext_arch  %{nil}
%else  ${_target_cpu} == noarch
%define _ext_arch .aarch64
%endif

%define _binaries_in_noarch_packages_terminate_build   0
%define _topdir             %(echo $HOME)/rpmbuild
%define _smp_mflags         -j8
%define __arch_install_post /usr/lib/rpm/check-buildroot
%define debug_package       %{nil}
%define _cross /opt/rce/package/x86_64/cross-arm64-gcc7/7.3/bin/aarch64-unknown-linux-gnu-
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

#----------------------------------------------------------
#--- USER-MODIFIABLE CONFIGS ---
#----------------------------------------------------------
%define pkgname    xilinx_kernel
%define version     4.14.0
%define vendor      rce
%define release     1
%define __prefix    /opt/rce/package/aarch64/%{pkgname}/%{version}

#----------------------------------------------------------
#--- SUMMARY ---
#----------------------------------------------------------
Summary: xilinx kernel for Zynq
Name: %{vendor}_%{pkgname}_arm64_%{version}
Version: %{version}
Release: %{release}%{_ext_arch}
Vendor: %{vendor}
License: GNU
Group: Kernel
Source: http://crosstool-ng.org
BuildRoot: %{_tmppath}/%{pkgname}-%{version}-root
BuildRequires: glibc-devel, rce_cross-arm64-gcc7 == 7.3
Prefix: %{__prefix}
AutoReq: no
BuildArch: noarch
%description
Xilinx Linux kernel

%changelog
* Tue Aug 21 2018 wittgen <wittgen@slac.stanford.edu>  [2018.2]
- Initial RPM release

#----------------------------------------------------------
#--- PREP ---
#----------------------------------------------------------

%prep
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p  $RPM_BUILD_ROOT/
cd $RPM_BUILD_ROOT/
git clone -b slac-%{version} https://github.com/slaclab/linux-xlnx.git
cd $RPM_BUILD_ROOT/linux-xlnx
cp %{_topdir}/SOURCES/zcu102_defconfig $RPM_BUILD_ROOT/linux-xlnx/arch/arm64/configs/

#----------------------------------------------------------
#--- BUILD ---
#----------------------------------------------------------
%build
%define _cross_prefix /opt/rce/rootfs/centos7_64
cd $RPM_BUILD_ROOT/linux-xlnx
rm -f .config
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/images
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/dtb
mkdir -p $RPM_BUILD_ROOT/%{__prefix}/build
make ARCH=arm64  KBUILD_DEFCONFIG=zcu102_defconfig defconfig
make ARCH=arm64 CROSS_COMPILE=%{_cross} -j24
make ARCH=arm64 CROSS_COMPILE=%{_cross} modules -j24
make ARCH=arm64  CROSS_COMPILE=%{_cross} EXTRA_CFLAGS="-B%{_cross_prefix}/opt/rh/devtoolset-7/root/lib/gcc/aarch64-redhat-linux/7  --sysroot=%{_cross_prefix}" EXTRA_LDFLAGS="--sysroot=%{_cross_prefix}"  -C tools/perf
dtc -I dts %{_topdir}/SOURCES/zcu102.dts -O dtb -i $RPM_BUILD_ROOT/linux-xlnx/arch/arm64/boot/dts/ -o $RPM_BUILD_ROOT/%{__prefix}/dtb/zcu102.dtb
cp  $RPM_BUILD_ROOT/linux-xlnx/arch/arm64/boot/Image  $RPM_BUILD_ROOT/%{__prefix}/images
cp -r %{_topdir}/SOURCES/boot   $RPM_BUILD_ROOT/%{__prefix}/images

#----------------------------------------------------------
#--- INSTALL ---
#----------------------------------------------------------
cd $RPM_BUILD_ROOT/linux-xlnx
make ARCH=arm64 CROSS_COMPILE=%{_cross} INSTALL_MOD_PATH=$RPM_BUILD_ROOT/%{__prefix} modules_install
make ARCH=arm64 CROSS_COMPILE=%{_cross} INSTALL_HDR_PATH=$RPM_BUILD_ROOT/%{__prefix} headers_install
make clean
rm -rf $(find  $RPM_BUILD_ROOT/linux-xlnx   -name ".git*" )
cp -r $RPM_BUILD_ROOT/linux-xlnx/* $RPM_BUILD_ROOT/%{__prefix}/build
rm -rf $RPM_BUILD_ROOT/linux-xlnx
rm -f $(find $RPM_BUILD_ROOT/${__prefix} -type l)


%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

#----------------------------------------------------------
#--- FILES ---
#----------------------------------------------------------
%files
%defattr(-,root,root)
%{__prefix}/*



