#!/usr/bin/env python
import os
import sys

pythonpath=""

try:
    pythonpath=os.environ['PYTHONPATH']
except:
    pass
 
# Set the directory you want to start from
if len(sys.argv) != 2: exit
rootDir = sys.argv[1]
ps=pythonpath.split(":")

dirs= pythonpath


for dirName, subdirList, fileList in os.walk(rootDir):
    if os.path.basename(dirName) == 'python':
        if os.path.isdir(dirName) and dirName not in ps:
            dirs=(dirName+":"+dirs).rstrip(":")
print dirs
