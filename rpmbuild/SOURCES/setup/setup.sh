_sdkroot=$(dirname ${BASH_SOURCE})
_sdkroot=$(cd ${_sdkroot}/> /dev/null 2>&1 && pwd)

if [ -r $HOME/.rcesdk ]; then 
    _sdkver=$(cat $HOME/.rcesdk)
else
    _sdkver="current"
fi

_lib=$( ${_sdkroot}/sdk_info.py --lib ${_sdkver} )
_bin=$( ${_sdkroot}/sdk_info.py --bin ${_sdkver} )
_python=$( ${_sdkroot}/sdk_info.py  --python  ${_sdkver}  )

if [ -n "${_lib}" ] ; then
    export LD_LIBRARY_PATH="${_lib}"
fi

if [ -n "${_bin}" ]; then

    export PATH="${_bin}"
fi

if [ -n "${_python}" ]; then
    export PYTHONPATH="${_python}"
fi

alias rce_project_setup="source /opt/rce/project_setup.sh"


unset _lib
unset _path
unset __python
unset _sdkroot
unset _sdkver
