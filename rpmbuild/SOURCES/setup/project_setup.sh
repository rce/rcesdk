#!/bin/bash

if [ "$#" -eq 1 ]; then
    dir=$1
fi
if [ "$#" -eq 0 ]; then
   dir=$PWD
fi

if [ -d $dir ] ; then
  export PYTHONPATH=$(/opt/rce/python_path.py $dir)
fi


