#!/bin/bash
cpu=$(uname -p)
if [ $cpu == "armv7l" ] ; then
cpu="armv7hl"
fi
base=/opt/rce/package/$cpu/drivers/5.5.0.4.14.0
/usr/sbin/insmod $base/rcestream.ko cfgTxCount0=8 cfgTxCount2=8 cfgRxCount0=32 cfgRxCount2=32 cfgSize0=131072 cfgSize2=131072 cfgMode2=20
/usr/sbin/insmod $base/rce_memmap.ko
exit 0

